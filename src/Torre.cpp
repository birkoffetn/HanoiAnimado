#include"Torre.hpp"
#include<exception>

using namespace std;
using namespace sf;

vector<Color> Torre::colores= {
    Color::Red, Color::Blue, Color::Yellow, Color::Green, Color::White
};

Torre::Torre(float wTorre, float minWithDisco, float hDisco): 
    _wTorre(wTorre), _minWithDisco(minWithDisco), _hDisco(hDisco)
{

}

void Torre::updateMedidas(){
    setOrigin(_wTorre/2.0, 0.0);
    int i= 0;
    float deltaW;
    if(_stack.size()< 2) deltaW= _wTorre;
    else deltaW= (_wTorre- _minWithDisco)/(_stack.size()- 1.0f);
    for(auto& disco: _stack){
        disco.setPosition(getPosition().x, getPosition().y- i* _hDisco);
        disco.setSize({_wTorre- deltaW* i, _hDisco});
    }
}

void Torre::setMedidas(float wTorre, float minWithDisco, float hDisco){
    _wTorre= wTorre; 
    _minWithDisco= minWithDisco; 
    _hDisco= hDisco;
}

RectangleShape Torre::pop(){
    if(_stack.empty()) throw "Pila vacía";
    auto disco= _stack.back();
    _stack.pop_back();
    return disco;
}

void Torre::push(const RectangleShape& disco){
    _stack.push_back(disco);
    _stack.back().setPosition(getPosition().x, getPosition().y- _stack.size()* _hDisco);
}

void Torre::init(int nDiscos){
    float rango;
    if(nDiscos< 2) rango= _wTorre;
    else rango= (_wTorre- _minWithDisco)/(nDiscos- 1);
    _stack.clear();
    for(auto i= 0; i< nDiscos; i++){
        _stack.emplace_back();
        auto& top= _stack.back();
        top.setSize({_wTorre- i* rango, _hDisco});
        top.setOrigin(top.getSize().x/ 2, _hDisco);
        top.setPosition(getPosition().x, getPosition().y- i*_hDisco);
        top.setFillColor(colores[i% colores.size()]);
    }
}

void Torre::draw(sf::RenderTarget& render, sf::RenderStates states) const{
    for(const auto& disco: _stack){
        render.draw(disco, states.transform /*getTransform()*/);
    }
}

Vector2f Torre::getTopPosition() const{
    if(_stack.size()< 1) return getPosition();
    else{
        return {
            _stack.back().getPosition().x,
            _stack.back().getPosition().y- _hDisco
        };
    }
}
