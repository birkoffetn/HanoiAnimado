#include "Menu.hpp"
#include <iostream>

Menu::Menu(Hanoi &hanoi, sf::RenderWindow &render) : _hanoi(hanoi), _render(render)
{
    _state = Pause;
    _nDiscos = 3;
    _font.loadFromFile("fonts/Anonymous Pro B.ttf");
    for (auto &text : _options)
    {
        text.setFont(_font);
        text.setCharacterSize(25);
    }
    _options[Pause].setString("Iniciar");
    _options[Pause].setPosition(100, 550);
    _options[Restart].setString("Reiniciar");
    _options[Restart].setPosition(300, 550);
    _options[Discos].setString("N = 3");
    _options[Discos].setPosition(500, 550);
    _options[Salir].setString("Salir");
    _options[Salir].setPosition(700, 550);
    changeColor();
    _hanoi.restart(3);
    _hanoi.pause();
    _isPaused = true;
}

void Menu::discos()
{
    std::string cadena = "N = ";
    cadena += std::to_string(_nDiscos);
    _options[Discos].setString(cadena);
}

void Menu::next()
{
    if (_state == Salir)
        _state = Pause;
    else
        _state++;
    changeColor();
}

void Menu::previus()
{
    if (_state == Pause)
        _state = Salir;
    else
        _state--;
    changeColor();
}

void Menu::execute()
{
    switch (_state)
    {
    case Options::Pause:
        if (_isPaused)
        {
            _isPaused = false;
            _hanoi.continuar();
            _options[Pause].setString("Pausar");
        }
        else
        {
            _isPaused = true;
            _hanoi.pause();
            _options[Pause].setString("Continuar");
        }
        break;
    case Options::Restart:
        _hanoi.restart(_nDiscos);
        _hanoi.pause();
        _options[Pause].setString("Iniciar");
        _isPaused = true;
        break;
    case Options::Discos:
        break;
    case Options::Salir:
        _render.close();
    default:
        break;
    }
}

void Menu::read(sf::Keyboard::Key code)
{
    switch (code)
    {
    case sf::Keyboard::Key::Left:
        previus();
        break;
    case sf::Keyboard::Key::Right:
        next();
        break;
    case sf::Keyboard::Key::Up:
        if (_state == Discos)
        {
            if (_nDiscos < 11)
                _nDiscos++;
        }
        discos();
        break;
    case sf::Keyboard::Key::Down:
        if (_state == Discos)
        {
            if (_nDiscos > 2)
                _nDiscos--;
        }
        discos();
        break;
    case sf::Keyboard::Key::Enter:
        execute();
        break;
    case sf::Keyboard::Key::Escape:
        _render.close();
        break;
    default:
        break;
    }
}

void Menu::changeColor()
{
    for (int i = 0; i < Options::Count; i++)
    {
        if (i == _state)
            _options[i].setFillColor(sf::Color::Red);
        else
            _options[i].setFillColor(sf::Color::Blue);
    }
}

void Menu::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    for (auto const &texto : _options)
    {
        target.draw(texto, states.Default);
    }
}

void Menu::mouseMoved(sf::Event::MouseMoveEvent pos)
{
    for (int i = 0; i < Options::Count; i++)
    {
        if (_options[i].getGlobalBounds().contains(pos.x, pos.y))
        {
            _state = (Options)i;
            changeColor();
            break;
        }
    }
}

void Menu::mousePressed(sf::Event::MouseButtonEvent button)
{
    for (int i = 0; i < Options::Count; i++)
    {
        if (_options[i].getGlobalBounds().contains(button.x, button.y))
        {
            if (i == Discos)
            {
                if (button.button == 0)
                {
                    if (_state == Discos)
                    {
                        if (_nDiscos < 11)
                            _nDiscos++;
                        discos();
                    }
                }
                else
                {
                    if (_state == Discos)
                    {
                        if (_nDiscos > 2)
                            _nDiscos--;
                        discos();
                    }
                }
            }
            execute();
            break;
        }
    }
}
