#include"Animation.hpp"

using namespace sf;

Animation::Animation(sf::Transformable& t): _pT(&t), _currentTime(0.0), _totalTime(0.0) { }

void Animation::iniciar(sf::Transformable& t, const sf::Vector2f& final, float time){
    _pT= &t;
    _begin= _pT->getPosition();
    _end= final;
    _currentTime= 0.0;
    _totalTime= time;
    _velocity= _end- _pT->getPosition();
    _velocity.x/= _totalTime;
    _velocity.y/= _totalTime;
}

void Animation::move(float deltaTime){
    if (isOver()){
        _pT->setPosition(_end);
    } else{
        _currentTime+= deltaTime;
        Vector2f desplazamiento;
        desplazamiento.x= _velocity.x* _currentTime;
        desplazamiento.y= _velocity.y* _currentTime;
        _pT->setPosition(_begin+ desplazamiento);
    }
}

bool Animation::isOver() const{
    return _totalTime< _currentTime;
}

const sf::Vector2f& Animation::getPosition() const{
    return _pT->getPosition();
}
