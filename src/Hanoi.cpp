#include "Hanoi.hpp"
#include <iostream>

using namespace std;
using namespace sf;

Hanoi::Hanoi() : _animacion(_disco)
{
    _ubicaciones[Torres::ORIGEN] = {100, 300};
    _ubicaciones[Torres::AUXLIAR] = {400, 300};
    _ubicaciones[Torres::DESTINO] = {700, 300};

    _colores.clear();
    _colores.emplace_back(sf::Color::White);
    _colores.emplace_back(sf::Color::Red);
    _colores.emplace_back(sf::Color::Green);
    _colores.emplace_back(sf::Color::Blue);
    _colores.emplace_back(sf::Color::Yellow);
    _colores.emplace_back(sf::Color::Magenta);
    _diccionario['o'] = Torres::ORIGEN;
    _diccionario['a'] = Torres::AUXLIAR;
    _diccionario['d'] = Torres::DESTINO;
    _isPaused = true;
}

void Hanoi::inicializar(int n)
{
    const float DELTA = 80.0 / (n - 1);
    auto &origen = _torres[Torres::ORIGEN];
    origen.clear();

    _torres[Torres::AUXLIAR].clear();
    _torres[Torres::DESTINO].clear();

    const auto &posicion = _ubicaciones[Torres::ORIGEN];

    for (auto i = 0; i < n; i++)
    {
        float radio = 100.0 - i * DELTA;
        origen.emplace_back(radio);
        origen.back().setOrigin(radio, radio);
        origen.back().setPosition(posicion);
        origen.back().setFillColor(_colores[i % _colores.size()]);
        origen.back().setOutlineColor(sf::Color::Black);
        origen.back().setOutlineThickness(1);
    }
    getSolucion(n);
    _currentMov = getSolucion(n).begin();

    _disco = _torres[Torres::ORIGEN].back();
    _torres[Torres::ORIGEN].pop_back();
    const auto &posFinal = _ubicaciones[_diccionario.at(_solucion.begin()->second)];
    _animacion.iniciar(_disco, posFinal);
    _isPaused = false;
}

void Hanoi::pause()
{
    _isPaused = true;
}

void Hanoi::restart(int n)
{
    inicializar(n);
}

void Hanoi::draw(RenderTarget &target, RenderStates states) const
{
    for (const auto &torre : _torres)
    {
        for (const auto &disco : torre)
        {
            target.draw(disco, states.Default);
        }
    }
    if (_animacion.isOver())
    {
    }
    else
    {
        target.draw(_disco);
    }
}

const vector<pair<char, char>> &Hanoi::getSolucion(int n)
{
    _solucion.clear();
    hanoiRecursivo(n, 'o', 'a', 'd');
    return _solucion;
}

void Hanoi::hanoiRecursivo(int n, const char &o, const char &a, const char &d)
{
    if (n == 1)
        _solucion.emplace_back(o, d);
    else
    {
        hanoiRecursivo(n - 1, o, d, a);
        _solucion.emplace_back(o, d);
        hanoiRecursivo(n - 1, a, o, d);
    }
}

bool Hanoi::run(float deltaTime)
{
    if (_isPaused)
        return false;
    if (_currentMov == _solucion.end())
        return false;

    //const auto& posOrigen= _ubicaciones[_diccionario.at(_currentMov->first)];
    const auto &posDestino = _ubicaciones[_diccionario.at(_currentMov->second)];
    auto &torreOrigen = _torres[_diccionario.at(_currentMov->first)];
    auto &torreDestino = _torres[_diccionario.at(_currentMov->second)];

    if (_animacion.isOver())
    {
        assert(torreOrigen.empty() != true);
        _disco = torreOrigen.back();
        torreOrigen.pop_back();
        _animacion.iniciar(_disco, posDestino);
    }
    else
    {
        _animacion.move(deltaTime);
        if (_animacion.isOver())
        {
            torreDestino.push_back(_disco);
            torreDestino.back().setPosition(posDestino);
            _currentMov++;
        }
    }
    return true;
}

void Hanoi::continuar()
{
    _isPaused = false;
}
