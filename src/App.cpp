#include "App.hpp"
#include <iostream>

App::App(sf::RenderWindow &render) : _render(render), _hanoi(), _menu(_hanoi, render)
{
    if (!_font.loadFromFile("fonts/FreeSerifBold.ttf"))
    {
        std::cout << "Fuente no ha sido caragada con exito" << std::endl;
    }
    _titulo.setFont(_font);
    _titulo.setString("Torres de Hanoi - Recursivo");
    _titulo.setFillColor(sf::Color::Cyan);
    _titulo.setPosition(150, 10);
    _titulo.setCharacterSize(50);
}

void App::run()
{
    _clock.restart();
    sf::Event event;

    while (_render.isOpen())
    {
        _render.clear();
        while (_render.pollEvent(event))
            readInput(event);
        updateLogic(_clock.restart().asSeconds());
        drawAll();
        _render.display();
    }
}

void App::drawAll()
{
    _render.draw(_hanoi);
    _render.draw(_titulo);
    _render.draw(_menu);
}

void App::readInput(sf::Event &event)
{
    if (event.type == sf::Event::Closed)
        _render.close();
    if (event.type == sf::Event::KeyPressed)
    {
        _menu.read(event.key.code);
    }
    if (event.type == sf::Event::MouseMoved)
    {
        _menu.mouseMoved(event.mouseMove);
    }
    if (event.type == sf::Event::MouseButtonPressed)
    {
        _menu.mousePressed(event.mouseButton);
    }
}

void App::updateLogic(float deltaTime)
{
    //std::cout<<"--> update logic"<<std::endl;
    _hanoi.run(deltaTime);
}
