#ifndef __TORRE__HPP__
#define __TORRE__HPP__

#include<SFML/Graphics.hpp>
#include<vector>

class Torre: public sf::Drawable, public sf::Transformable{
    public:
        Torre(float wTorre= 100.0, float minWithDisco= 20.0, float hDisco= 30.0);
        void setMedidas(float wTorre, float minWithTorre, float hDisco);
        void init(int nDiscos);

        sf::RectangleShape pop();
        void push(const sf::RectangleShape& disco);
        sf::Vector2f getTopPosition() const;
    protected:
        void draw(sf::RenderTarget& render, sf::RenderStates states)const override;
        void updateMedidas();
    private:
        std::vector<sf::RectangleShape> _stack;
        float                           _wTorre, _minWithDisco, _hDisco;

        static std::vector<sf::Color>   colores;
};

#endif