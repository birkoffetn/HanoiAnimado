#ifndef __menu__hpp__
#define __menu__hpp__

#include <SFML/Graphics.hpp>
#include "Hanoi.hpp"

class Menu : public sf::Drawable
{
public:
    enum Options
    {
        Pause,
        Restart,
        Discos,
        Salir,
        Count
    };
    Menu(Hanoi &hanoi, sf::RenderWindow &render);
    void next();
    void previus();
    void execute();
    void changeColor();
    void discos();
    void read(sf::Keyboard::Key code);
    void mouseMoved(sf::Event::MouseMoveEvent pos);
    void mousePressed(sf::Event::MouseButtonEvent button);

protected:
    virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

private:
    Hanoi &_hanoi;
    sf::RenderWindow &_render;
    sf::Text _options[Options::Count];
    sf::Font _font;
    int _state;
    int _nDiscos;
    bool _isPaused;
};

#endif
