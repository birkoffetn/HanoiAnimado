#ifndef __APP__HPP__
#define __APP__HPP__

#include<SFML/Graphics.hpp>
#include"Torre.hpp"
#include"Hanoi.hpp"
#include"Animation.hpp"
#include"Torre.hpp"
#include"Menu.hpp"

class App{
    public:
        App(sf::RenderWindow& render);
        void run();
    protected:
        void readInput(sf::Event& event);
        void updateLogic(float time);
        void drawAll();
    private:
        sf::RenderWindow&   _render;
        sf::Clock           _clock;
        Hanoi               _hanoi;
        Menu                _menu;
        sf::Font            _font;
        sf::Text            _titulo;
};

#endif
