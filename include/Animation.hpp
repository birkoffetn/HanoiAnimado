#ifndef __ANIMATION__HPP__
#define __ANIMATION__HPP__

#include<SFML/Graphics.hpp>

class Animation{
    public:
        Animation(sf::Transformable& t);
        void iniciar(sf::Transformable& t, const sf::Vector2f& final, float time= 1.0);
        void move(float deltaTime);
        bool isOver() const;
        const sf::Vector2f& getPosition() const;
    protected:
    private:
        sf::Transformable*  _pT;
        float               _currentTime, _totalTime;
        sf::Vector2f        _begin, _end, _velocity;
};

#endif