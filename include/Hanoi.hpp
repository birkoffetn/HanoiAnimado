#ifndef __HANOI__HPP__
#define __HANOI__HPP__

#include <SFML/Graphics.hpp>
#include <map>
#include <vector>
#include <assert.h>
#include <Animation.hpp>

class Hanoi : public sf::Drawable, sf::Transformable
{
public:
    enum Torres
    {
        ORIGEN,
        AUXLIAR,
        DESTINO,
        COUNT
    };
    Hanoi();
    const std::vector<std::pair<char, char>> &getSolucion(int n);
    void inicializar(int n);
    void pause();
    void continuar();
    void restart(int nDiscos);
    bool run(float deltaTime);

protected:
    void draw(sf::RenderTarget &target, sf::RenderStates states) const;

private:
    void hanoiRecursivo(int n, const char &o, const char &a, const char &d);

    std::vector<std::pair<char, char>> _solucion;
    std::vector<std::pair<char, char>>::const_iterator _currentMov;
    std::vector<sf::CircleShape> _torres[Torres::COUNT];
    sf::Vector2f _ubicaciones[Torres::COUNT];
    std::vector<sf::Color> _colores;
    Animation _animacion;
    sf::CircleShape _disco;
    bool _isPaused;
    std::map<char, Torres> _diccionario;
};

#endif
