OBJECTS= $(patsubst src/%.cpp,obj/%.o,$(wildcard src/*.cpp))
BIN= bin/hanoi
CXX= g++
CXXFLAGS= -Iinclude -Wall -Wextra

all: $(BIN)
	./$(BIN)

obj/%.o: src/%.cpp include/%.hpp
	$(CXX) -o $@ -c $< $(CXXFLAGS)

$(BIN): $(OBJECTS)
	$(CXX) -o $@ $^ -lsfml-window -lsfml-system -lsfml-graphics -lsfml-audio -lsfml-network

run: $(BIN)
	./$(BIN)

clean:
	rm -f $(BIN) $(OBJECTS)
